## 巨量IP  API  SDK  -  Node.js

通过SDK可快速调用巨量IP支持的API接口,  [查看详情](https://gitee.com/juliangip/juliang-nodejs-sdk/blob/master/README.md)

### <u>简介</u>

巨量IP - Api SDK (NodeJs 版本)

### <u>依赖环境</u>

1. sync-request

   ```NodeJs
   npm install sync-request
   ```

2. express (运行测试案例时需要安装,如果不需要测试则无需安装)

   ```NodeJS
   npm install express
   ```

3. 从[巨量IP](https://www.juliangip.com/)购买相应的产品

4. 获取订单的[trade_no(业务号)](http://www.juliangip.com/users/product/time) 和对应的 [Key(业务秘钥)](http://www.juliangip.com/users/product/quantity)

### <u>获取安装</u>

安装nodeJs SDK之前, 请先获取订单所对应的业务编号(trade_no)和业务对应的秘钥(Key). 订单编号时用于识别具体业务订单, Key 则是用于加密签名字符串和服务器端用于验证签名一致性的秘钥. 因此,业务秘钥必须严格保管,避免泄露,一旦泄露,请立即前往个人中心 ——>[产品管理](http://www.juliangip.com/users/product/time) 页面进行对应业务订单的秘钥重置.

### <u>通过npm安装(推荐)</u>

您可以通过npm将SDK安装到您的项目中:

```NodeJs
npm install juliangip
```

如果您的项目环境未配置npm,可前往[NodeJs](https://nodejs.org/zh-cn/) 官网下载并安装合适的NodeJs版本,然后可参考下方示例进行安装:

- Ubuntu 安装npm : 

  ```shell
  1.获取安装源,在终端中执行
  curl -sL https://deb.nodesource.com/setup_17.0.1 | sudo -E bash -
  2.等待源添加完毕后在执行
  sudo apt-get install -y nodejs
  3.验证安装
  node -v:查看node版本
  ```

- CentOS安装npm : 

  ```shell
  1.使用wget拉取源文件
  wget https://npm.taobao.org/mirrors/node/v17.0.1/node-v17.0.1-linux-x64.tar.gz
  2.执行tar -zxvf 执行解压缩命令
  tar -xvf node-v17.0.1-linux-x64.tar.gz
  3.验证安装
  node -v:查看node版本
  ```

- MacOS安装npm : 

  ```shell
  1.打开终端，执行以下命令安装Homebrew
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  2.安装node
  brew install node   //sudo 使用admin权限
  3.验证安装
  node -v:查看node版本
  ```



### <u>通过源码安装</u>

前往Gitee代码托管地址 [巨量云 - NodeJs-sdk](https://gitee.com/juliangip/juliang-nodejs-sdk.git) 下载最新的代码,解压后放置在需要使用的工程目录下即可

### 启动命令

```javascript
$ cd sdk
$ npm i sync-request
$ npm i express
$ node API/server.js (执行开启http端口监听的js文件)
```



### <u>示例</u>

```js
/**
 * juliang --node sdk使用示例
 * @author http://www.juliangip.com
 */
//导入 Node.js Web 应用程序开发框架 -- express
const express = require('express');
const app = express();
//导入巨量IP -- sdk
const juliang = require('juliangip');
//创建Api服务调用对象client
const client = new juliang.client();

var result = ''; //用于接收返回值的参数

//初始化服务调用所需公共必要参数
const user_id = '用户ID';
const accessKey = 'API秘钥';
const dyTradeNo = "动态代理 -- 业务编号";
const dyKey = "业务秘钥";

//启动程序监听3600端口<如端口冲突 , 可自行修改端口>
app.listen(3600,()=>{
    console.log('server start --> port : 3600')
})

app.get('/',(req,resp)=> {
    resp.send('巨量IP API 示例')
})

app.get('/users/balance',(req,resp)=>{
    /**
	 * 公共接口 -- 获取账户余额
	 * @param {userId,AccessKey} {用户ID,秘钥}
 	 * @param {HTTP请求方式} method 
	 */
	result = client.userBalance({userId: user_id,AccessKey: accessKey});
	console.log(result);
	resp.send(result);
    
})

app.use('/dynamic/getIps',(req,resp)=> {
    /**
	 * 动态代理 -- 提取代理IP示例
	 * @param {业务秘钥} key <必传参数>
     * @param {业务号} trade_no <必传参数>
     * @param {提取数量} num <必传参数>
     * @param {可选参数
     *  pt : 代理类型,
     *  result_type : 返回类型('text' | 'json' | 'xml')
     *  split : 结果分隔符,
     *  city_name : 地区名称,
     *  city_code : 邮政编码,
     *  ip_remain : 剩余可用时长,
     *  area : 筛选地区,
     *  no_area : 排除地区,
     *  ip_seg : 筛选IP段,
     *  no_ip_seg : 排除IP段,
     *  isp : 运营商筛选,
     *  filter : IP去重
     * } otherParams <可选参数>
     * @param {HTTP请求方式} method 
	 */
	result = client.dynamicGetIps(dyKey,dyTradeNo,5,{pt: 2,result_type: 'json',city_name: 1});
	console.log(result);
    resp.send(result);
})
```

您可以在API下的[server.js](https://gitee.com/juliangip/juliang-nodejs-sdk/blob/master/sdk/API/server.js) 文件中找到完整的示例

### <u>参考资料</u>

- [产品文档](http://www.juliangip.com/help/product/dynamic/)
- [快速入门](http://www.juliangip.com/help/apply/rm/)
- [API接口](http://www.juliangip.com/help/api/api/)
- [开发者指南](http://www.juliangip.com/help/dev/dev/)

