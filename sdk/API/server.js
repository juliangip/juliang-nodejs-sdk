const express = require('express');
const app = express();

// 引入body-parser模块
const bodyParser = require('body-parser');
const Client = require('./clients');
var client = new Client()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}));
const user_id = 'your user_id';
const accessKey = 'your AccessKey';
const dyTradeNo = 'dynamic trade_no';
const dyKey = 'dynamic accesskey';
const kpsTradeNo = 'alone trade_no';
const kpsKey = 'alone accesskey';
const unlimitedTradeNo = 'unlimited trade_no';
const unlimitedKey = 'unlimited accesskey';
const postPayTradeNo = 'postpay trade_no';
const postPayKey = 'unlimited accesskey';
const companyPostPayTradeNo = 'companyPostPayTradeNo trade_no';
const companyPostPayKey = 'companyPostPayKey accesskey';
const companyDynamicTradeNo = 'companyDynamicTradeNo trade_no';
const companyDynamicKey = 'companyDynamicKey accesskey';


var result = ''; //用于接收返回值的参数

//启动程序监听3600端口
app.listen(3600, () => {
    console.log('server start --> port : 3600')
})

app.get('/', (req, resp) => {
    resp.send('巨量IP API 示例')
})

app.get('/users/balance', (req, resp) => {
    /**
     * 账户接口 -- 获取账户余额
     */
    result = client.userBalance(user_id, accessKey);
    console.log(result);
    resp.send(result);

})

app.use('/users/getAllOrders/:type', (req, resp) => {
    /**
     *  账户接口 -- 获取所有正常订单相关信息
     */
    result = client.getAllOrders(user_id, accessKey, req.params.type, {show: '1'});
    console.log(result);
    resp.send(result);
})

app.use('/users/getCity', (req, resp) => {
    /**
     * 账户接口 -- 获取对应省份下的可用代理城市
     */
    result = client.getCity(user_id, accessKey, '山东,河北')
    console.log(result)
    resp.send(result)
})

app.use('/dynamic/getIps', (req, resp) => {
    /**
     * 动态代理 -- 提取代理IP示例
     */
    result = client.dynamicGetIps(dyKey, dyTradeNo, 5, {pt: 2, result_type: 'json', city_name: 1});
    console.log(result);
    resp.send(result);
})


app.use('/dynamic/check', (req, resp) => {
    /**
     * 动态代理 -- 校验代理IP可用性
     */
    result = client.dynamicCheck(dyKey, dyTradeNo, '140.237.15.37:42194,61.131.45.137:34846');
    console.log(result);
    resp.send(result);
})

app.use('/dynamic/setWhiteIp', (req, resp) => {

    /**
     * 动态代理 -- 设置代理IP白名单
     */
    result = client.dynamicSetWhiteIp(dyKey, dyTradeNo, '36.36.36.36,5.56.32.11');
    console.log(result);
    resp.send(result);
})


app.use('/dynamic/getWhiteIp', (req, resp) => {
    /**
     * 动态代理 -- 获取IP白名单
     */
    result = client.dynamicGetWhiteIp(dyKey, dyTradeNo);
    console.log(result);
    resp.send(result);
})

app.use('/dynamic/replace', (req, resp) => {
    /**
     * 动态代理 -- 替换IP白名单
     */
    result = client.dynamicReplace(dyKey, dyTradeNo, "99.99.99.99,8.8.8.8", {old_ip: "6.6.6.6", reset: "1"});
    // result = client.dynamicReplace(dyKey,dyTradeNo,"6.6.6.69,65.85.95.12,5.5.65.5",{old_ip: "6.6.6.6"});
    console.log(result);
    resp.send(result);
})

app.use('/dynamic/remain', (req, resp) => {
    /**
     * 动态代理 -- 获取代理剩余可用时长
     */
    result = client.dynamicRemain(dyKey, dyTradeNo, '171.35.169.70:33657,218.23.195.37:31616');
    console.log(result);
    resp.send(result);
})


app.use('/dynamic/balance', (req, resp) => {
    /**
     * 动态代理 -- 获取剩余可提取IP数量
     */
    result = client.dynamicBalance(dyKey, dyTradeNo);
    console.log(result);
    resp.send(result);
})

app.use('/alone/getIps', (req, resp) => {
    /**
     * 独享代理 -- 获取代理详情
     */
    result = client.aloneGetIps(kpsKey, kpsTradeNo, {
        sock_port: 1,
        city_name: 1,
        ip_remain: 1,
        city_code: 1,
        order_endtime: 1
    });
    console.log(result);
    resp.send(result);
})

app.use('/alone/setWhiteIp', (req, resp) => {
    /**
     * 独享代理 -- 设置IP白名单
     */
    result = client.aloneSetWhiteIp(kpsKey, kpsTradeNo, '36.36.36.36,66.66.66.66')
    console.log(result);
    resp.send(result);
})

app.use('/alone/getWhiteIp', (req, resp) => {
    /**
     * 独享代理 -- 获取代理IP白名单
     */
    result = client.aloneGetWhiteIp(kpsKey, kpsTradeNo);
    console.log(result);
    resp.send(result);
})


app.use('/alone/replace', (req, resp) => {
    /**
     * 独享代理 -- 替换IP白名单
     */
    // result = client.aloneReplace(kpsKey,kpsTradeNo,"99.99.99.99,8.8.8.8",{old_ip: "6.6.6.6,7.7.6.3",reset:"1"});
    result = client.aloneReplace(kpsKey, kpsTradeNo, "3.3.3.3,6.6.6.6,9.6.6.3,5.5.2.3", {old_ip: "8.8.8.8"});
    console.log(result);
    resp.send(result);
})

app.use('/unlimited/getips', (req, resp) => {
    /**
     * 不限量 -- 获取IP
     */
    result = client.unlimitedGetIps(unlimitedKey, unlimitedTradeNo, 5, "POST");
    console.log(result);
    resp.send(result);
})

app.use('/unlimited/setwhiteip', (req, resp) => {
    /**
     * 不限量 -- 设置IP白名单
     */
    result = client.unlimitedSetWhiteIp(unlimitedKey, unlimitedTradeNo, "223.6.6.6", "POST");
    console.log(result);
    resp.send(result);
})

app.use('/unlimited/getwhiteip', (req, resp) => {
    /**
     * 不限量 -- 获取IP白名单
     */
    result = client.unlimitedGetWhiteIp(unlimitedKey, unlimitedTradeNo, "POST");
    console.log(result);
    resp.send(result);
})

app.use('/unlimited/replaceWhiteIp', (req, resp) => {
    /**
     * 不限量 -- 替换IP白名单
     */
    result = client.unlimitedReplaceWhiteIp(unlimitedKey, unlimitedTradeNo, "223.6.6.6", {old_ip: "223.6.6.6"}, "POST");
    console.log(result);
    resp.send(result);
})

app.use('/postpay/getips', (req, resp) => {
    /**
     * 按量付费 -- 获取IP
     */
    result = client.postPayGetIps(postPayKey, postPayTradeNo, 5, {pt: 2, result_type: 'json', city_name: 1}, "POST");
    console.log(result);
    resp.send(result);
})

app.use('/postpay/check', (req, resp) => {
    /**
     * 按量付费 -- 检测IP
     */
    result = client.postPayCheck(postPayKey, postPayTradeNo, "140.237.15.37:42194", "POST");
    console.log(result);
    resp.send(result);
})

app.use('/postpay/setwhiteip', (req, resp) => {
    /**
     * 按量付费 -- 设置IP白名单
     */
    result = client.postPaySetWhiteIp(postPayKey, postPayTradeNo, "223.6.6.6", "POST");
    console.log(result);
    resp.send(result);
})

app.use('/postpay/getwhiteip', (req, resp) => {
    /**
     * 按量付费 -- 获取IP白名单
     */
    result = client.postPayGetWhiteIp(postPayKey, postPayTradeNo, "POST");
    console.log(result);
    resp.send(result);
})

app.use('/postpay/replaceWhiteIp', (req, resp) => {
    /**
     * 按量付费 -- 替换IP白名单
     */
    result = client.postPayReplaceWhiteIp(postPayKey, postPayTradeNo, "223.6.6.6",{old_ip:"223.6.6.6"}, "POST");
    console.log(result);
    resp.send(result);
})


app.use('/company/postpay/getips', (req, resp) => {
    /**
     * 按量付费(企业版) -- 获取IP
     */
    result = client.CompanyPostPayGetIps(companyPostPayKey, companyPostPayTradeNo, 1, {pt: 2, result_type: 'json'}, "POST");
    console.log(result);
    resp.send(result);
})

app.use('/company/postpay/setwhiteip', (req, resp) => {
    /**
     * 按量付费(企业版) -- 设置IP白名单
     */
    result = client.CompanyPostPaySetWhiteIp(companyPostPayKey, companyPostPayTradeNo, "223.6.6.6", "POST");
    console.log(result);
    resp.send(result);
})

app.use('/company/postpay/getwhiteip', (req, resp) => {
    /**
     * 按量付费(企业版) -- 获取IP白名单
     */
    result = client.CompanyPostPayGetWhiteIp(companyPostPayKey, companyPostPayTradeNo, "POST");
    console.log(result);
    resp.send(result);
})

app.use('/company/postpay/delWhiteIp', (req, resp) => {
    /**
     * 按量付费(企业版) -- 删除IP白名单
     */
    result = client.CompanyPostPayDelWhiteIp(companyPostPayKey, companyPostPayTradeNo, "223.6.6.6", "POST");
    console.log(result);
    resp.send(result);
})

app.use('/company/dynamic/getips', (req, resp) => {
    /**
     * 包量付费(企业版) -- 获取IP
     */
    result = client.CompanyDynamicGetIps(companyDynamicKey, companyDynamicTradeNo, 1, {pt: 2, result_type: 'json'}, "POST");
    console.log(result);
    resp.send(result);
})

app.use('/company/dynamic/setwhiteip', (req, resp) => {
    /**
     * 包量付费(企业版) -- 设置IP白名单
     */
    result = client.CompanyDynamicSetWhiteIp(companyDynamicKey, companyDynamicTradeNo, "223.6.6.6", "POST");
    console.log(result);
    resp.send(result);
})

app.use('/company/dynamic/getwhiteip', (req, resp) => {
    /**
     * 包量付费(企业版) -- 获取IP白名单
     */
    result = client.CompanyDynamicGetWhiteIp(companyDynamicKey, companyDynamicTradeNo, "POST");
    console.log(result);
    resp.send(result);
})

app.use('/company/dynamic/delWhiteIp', (req, resp) => {
    /**
     * 包量付费(企业版) -- 删除IP白名单
     */
    result = client.CompanyDynamicDelWhiteIp(companyDynamicKey, companyDynamicTradeNo, "223.6.6.6", "POST");
    console.log(result);
    resp.send(result);
})