/**
 * @file URL请求路径的常量和常用函数
 * @author www.juliang.com
 */

const crypto = require('crypto');


//API请求 域名+路径
const BASEURL = "http://v2.api.juliangip.com";

const ENDPOINT = {
    USERS_GETBALANCE : "/users/getbalance",
    USERS_GETALLORDERS: "/users/getAllOrders",
    USERS_GETCITY: "/users/getCity",
    DYNAMIC_GETIPS : "/dynamic/getips",
    DYNAMIC_CHECK : "/dynamic/check",
    DYNAMIC_SETWHITEIP : "/dynamic/setwhiteip",
    DYNAMIC_REPLACEWHITEIP : "/dynamic/replaceWhiteIp",
    DYNAMIC_GETWHITEIP : "/dynamic/getwhiteip",
    DYNAMIC_REMAIN : "/dynamic/remain",
    DYNAMIC_BALANCE : "/dynamic/balance",
    ALONE_GETIPS : "/alone/getips",
    ALONE_SETWHITEIP : "/alone/setwhiteip",
    ALONE_GETWHITEIP : "/alone/getwhiteip",
    ALONE_REPLACEWHITEIP : "/alone/replaceWhiteIp",
    UNLIMITED_GETIPS : "/unlimited/getips",
    UNLIMITED_SETWHITEIP : "/unlimited/setwhiteip",
    UNLIMITED_GETWHITEIP : "/unlimited/getwhiteip",
    UNLIMITED_REPLACEWHITEIP : "/unlimited/replaceWhiteIp",
    POSTPAY_GETIPS : "/postpay/getips",
    POSTPAY_CHECK : "/postpay/check",
    POSTPAY_SETWHITEIP : "/postpay/setwhiteip",
    POSTPAY_GETWHITEIP : "/postpay/getwhiteip",
    POSTPAY_REPLACEWHITEIP : "/postpay/replaceWhiteIp",
    COMPANY_POSTPAY_GETIPS : "/company/postpay/getips",
    COMPANY_POSTPAY_SETWHITEIP : "/company/postpay/setwhiteip",
    COMPANY_POSTPAY_GETWHITEIP : "/company/postpay/getwhiteip",
    COMPANY_POSTPAY_DELWHITEIP : "/company/postpay/delwhiteip",
    COMPANY_DYNAMIC_GETIPS : "/company/dynamic/getips",
    COMPANY_DYNAMIC_SETWHITEIP : "/company/dynamic/setwhiteip",
    COMPANY_DYNAMIC_GETWHITEIP : "/company/dynamic/getwhiteip",
    COMPANY_DYNAMIC_DELWHITEIP : "/company/dynamic/delwhiteip",
}



Object.freeze(ENDPOINT); //设置ENDPOINT无法被修改

/**
 * 
 * @param {访问域名} baseurl 
 * @param {接口路径} path 
 * @returns 
 */
function getUrl(path,baseurl = BASEURL){
    return baseurl+path;
}


/**
 * 
 * @param {{trade_no, ips, key}} params 对象数组
 * @returns URL请求参数
 */
function getQuery(params){
    
    let key = '';
    let cs = '';
    let str = '';
    Object.keys(params).sort().map(k =>{
        if(params[k] ==null || params[k] == '' ){
            return;
        }
        if(k == 'key'){
            key = params[k];
            return;
        }
        str += `${k}=${params[k]}&`; 
    })

    let signStr = str+'key='+key;
    let sign = crypto.createHash('md5').update(signStr).digest('hex').toLowerCase();
    str += 'sign='+sign
    return new URLSearchParams(str);
}


exports.ENDPOINT = ENDPOINT;
exports.getQuery = getQuery;
exports.getUrl = getUrl;





