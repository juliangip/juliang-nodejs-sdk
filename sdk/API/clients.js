/**
 * @file client 模块 --> 用于最终执行请求客户端
 * @author www.juliangip.com
 */



const utils = require('./UrlUtils');
var http = require('sync-request');
const endpoint = utils.ENDPOINT;


class Client {

    constructor() {

    }

    /**
     * 获取账户可用余额
     * @param {userId,AccessKey} {用户ID,秘钥}
     * @param {HTTP请求方式} method
     *
     */
    userBalance(userId, AccessKey, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.USERS_GETBALANCE);
        let params = {user_id: userId, key: AccessKey};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 获取用户对应订单类型中正常状态的订单
     * @param userId, 用户ID
     * @param AccessKey 秘钥
     * @param productType   产品类型
     * @param {可选参数:
     *  show : 是否返回秘钥
     * }
     * @param HTTP请求方式 method
     */
    getAllOrders(userId, AccessKey, productType, otherParams = {}, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.USERS_GETALLORDERS);
        let params = {user_id: userId, key: AccessKey, product_type: productType, ...otherParams};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 获取对应省份下的可用代理城市信息
     * @param userId, 用户ID
     * @param AccessKey 秘钥
     * @param province 省份名称
     */
    getCity(userId, AccessKey, province, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.USERS_GETCITY);
        let params = {user_id: userId, key: AccessKey, province};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params)
    }

    /**
     * 动态代理 -- 提取代理IP
     * @param {业务秘钥} key <必传参数>
     * @param {业务号} trade_no <必传参数>
     * @param {提取数量} num <必传参数>
     * @param {可选参数
     *  pt : 代理类型,
     *  result_type : 返回类型('text' | 'json' | 'xml')
     *  split : 结果分隔符,
     *  city_name : 地区名称,
     *  city_code : 邮政编码,
     *  ip_remain : 剩余可用时长,
     *  area : 筛选地区,
     *  no_area : 排除地区,
     *  ip_seg : 筛选IP段,
     *  no_ip_seg : 排除IP段,
     *  isp : 运营商筛选,
     *  filter : IP去重
     * } otherParams <可选参数>
     * @param {HTTP请求方式} method
     */
    dynamicGetIps(key, trade_no, num, otherParams = {}, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.DYNAMIC_GETIPS);
        let params = {key, trade_no, num, ...otherParams}
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 动态代理 -- 校验代理IP有效性
     * @param {业务秘钥} key
     * @param {业务号} trade_no
     * @param {代理IP列表} proxy
     * @param {HTTP请求方式} method
     * @returns
     */
    dynamicCheck(key, trade_no, proxy = [], method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.DYNAMIC_CHECK);
        let params = {key, trade_no, proxy}
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 动态代理 -- 设置IP白名单
     * @param {业务秘钥} key
     * @param {业务号} trade_no
     * @param {需要设置的白名单IP} ips
     * @param {HTTP请求类型} method
     * @returns
     */
    dynamicSetWhiteIp(key, trade_no, ips, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.DYNAMIC_SETWHITEIP);
        let params = {key, trade_no, ips};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 动态代理 -- 替换IP白名单
     * @param {*} key
     * @param {*} trade_no
     * @param {*} new_ip
     * @param {
     *     old_ip : 需要被替换的已存在的白名单
     *     reset : 是否全部重置
     * } otherParams <可选参数>
     * @param {*} method
     * @returns
     */
    dynamicReplace(key, trade_no, new_ip, otherParams = {}, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.DYNAMIC_REPLACEWHITEIP);
        let params = {key, trade_no, new_ip, ...otherParams}
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 动态代理 -- 获取IP白名单
     * @param {业务秘钥} key
     * @param {业务号} trade_no
     * @param {HTTP请求类型} method
     * @returns
     */
    dynamicGetWhiteIp(key, trade_no, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.DYNAMIC_GETWHITEIP);
        let params = {key, trade_no};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 动态代理 -- 获取剩余可用时长
     * @param {业务秘钥} key
     * @param {业务号} trade_no
     * @param {代理IP列表} proxy
     * @param {HTTP请求类型} method
     * @returns
     */
    dynamicRemain(key, trade_no, proxy, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.DYNAMIC_REMAIN);
        let params = {key, trade_no, proxy};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 动态代理 -- 获取剩余可提取IP数
     * @param {业务秘钥} key
     * @param {业务号} trade_no
     * @param {HTTP请求类型} method
     * @returns
     */
    dynamicBalance(key, trade_no, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.DYNAMIC_BALANCE);
        let params = {key, trade_no};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 独享代理 -- 获取代理详情
     * @param {业务秘钥} key
     * @param {业务号} trade_no
     * @param {选填参数列表
     *    sock_port : Sock代理端口,
     *    city_name : 地区名称,
     *    city_code : 邮政编码,
     *    ip_remain : IP可用时长(动态型独有)
     *    order_endtime : 业务到期时间
     * } otherParams
     * @param {HTTP请求类型} method
     * @returns
     */
    aloneGetIps(key, trade_no, otherParams = {}, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.ALONE_GETIPS);
        let params = {key, trade_no, ...otherParams};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 独享代理 -- 设置IP表名单
     * @param {业务秘钥} key
     * @param {业务号} trade_no
     * @param {白名单IP列表} ips
     * @param {HTTP请求类型} method
     * @returns
     */
    aloneSetWhiteIp(key, trade_no, ips, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.ALONE_SETWHITEIP);
        let params = {key, trade_no, ips};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 独享代理 -- 获取白名单
     * @param {业务秘钥} key
     * @param {业务号} trade_no
     * @param {HTTP请求类型} method
     * @returns
     */
    aloneGetWhiteIp(key, trade_no, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.ALONE_GETWHITEIP);
        let params = {key, trade_no};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 独享代理 -- 替换IP白名单
     * @param {业务密钥} key
     * @param {业务号} trade_no
     * @param {新的ip} new_ip
     * @param {
     *     old_ip : 需要被替换的已存在的白名单
     *     reset : 是否全部重置
     * } otherParams <可选参数>
     * @param {*} method
     * @returns
     */
    aloneReplace(key, trade_no, new_ip, otherParams = {}, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.ALONE_REPLACEWHITEIP);
        let params = {key, trade_no, new_ip, ...otherParams}
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 不限量获取IP
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param num 获取数量
     * @param method 请求方式
     */
    unlimitedGetIps(key, trade_no, num, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.UNLIMITED_GETIPS);
        let params = {key, trade_no, num};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 不限量 -- 设置白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param ips 白名单IP列表
     * @param method 请求方式
     * @returns {string|string|*}
     */
    unlimitedSetWhiteIp(key, trade_no, ips, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.UNLIMITED_SETWHITEIP);
        let params = {key, trade_no, ips};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 不限量 -- 获取白名单
     * @param key 业务密钥
     * @param trade_no  业务号
     * @param method 请求方式
     * @returns {string|string|*}
     */
    unlimitedGetWhiteIp(key, trade_no, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.UNLIMITED_GETWHITEIP);
        let params = {key, trade_no};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 不限量 -- 替换白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param new_ip 新的IP
     * @param otherParams <可选参数>
     * @param method 请求方式
     * @returns {string|string|*}
     */
    unlimitedReplaceWhiteIp(key, trade_no, new_ip, otherParams = {}, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.UNLIMITED_REPLACEWHITEIP);
        let params = {key, trade_no, new_ip, ...otherParams};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 获取IP
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param num 获取数量
     * @param otherParams <可选参数>
     * @param method 请求方式
     * @returns {string|string|*}
     */
    postPayGetIps(key, trade_no, num, otherParams = {}, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.POSTPAY_GETIPS);
        let params = {key, trade_no, num, ...otherParams};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 检查ip有效性
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param proxy 待检测ip
     * @param method 请求方式
     * @returns {*}
     */
    postPayCheck(key, trade_no, proxy = [], method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.POSTPAY_CHECK);
        let params = {key, trade_no,proxy};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 设置白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param ips 白名单
     * @param method 请求方式
     * @returns {string|string|*}
     */
    postPaySetWhiteIp(key, trade_no, ips, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.POSTPAY_SETWHITEIP);
        let params = {key, trade_no, ips};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 获取白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param method 请求方式
     * @returns {string|string|*}
     */
    postPayGetWhiteIp(key,trade_no,method = 'POST'){
        let apiUrl = utils.getUrl(endpoint.POSTPAY_GETWHITEIP);
        let params = {key, trade_no};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 替换白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param new_ip 新的IP
     * @param otherParams <可选参数>
     * @param method 请求方式
     * @returns {string|string|*}
     */
    postPayReplaceWhiteIp(key,trade_no,new_ip,otherParams = {},method = 'POST'){
        let apiUrl = utils.getUrl(endpoint.POSTPAY_REPLACEWHITEIP);
        let params = {key, trade_no, new_ip, ...otherParams};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 获取IP
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param num 获取数量
     * @param otherParams <可选参数>
     * @param method 请求方式
     * @returns {string|string|*}
     */
    CompanyPostPayGetIps(key, trade_no, num, otherParams = {}, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.COMPANY_POSTPAY_GETIPS);
        let params = {key, trade_no, num, ...otherParams};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 设置白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param ips 白名单
     * @param method 请求方式
     * @returns {string|string|*}
     */
    CompanyPostPaySetWhiteIp(key, trade_no, ips, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.COMPANY_POSTPAY_SETWHITEIP);
        let params = {key, trade_no, ips};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 获取白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param method 请求方式
     * @returns {string|string|*}
     */
    CompanyPostPayGetWhiteIp(key,trade_no,method = 'POST'){
        let apiUrl = utils.getUrl(endpoint.COMPANY_POSTPAY_GETWHITEIP);
        let params = {key, trade_no};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 替换白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param del_ip 新的IP
     * @param method 请求方式
     * @returns {string|string|*}
     */
    CompanyPostPayDelWhiteIp(key,trade_no,del_ip,method = 'POST'){
        let apiUrl = utils.getUrl(endpoint.COMPANY_POSTPAY_DELWHITEIP);
        let params = {key, trade_no, del_ip};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 获取IP
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param num 获取数量
     * @param otherParams <可选参数>
     * @param method 请求方式
     * @returns {string|string|*}
     */
    CompanyDynamicGetIps(key, trade_no, num, otherParams = {}, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.COMPANY_DYNAMIC_GETIPS);
        let params = {key, trade_no, num, ...otherParams};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 设置白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param ips 白名单
     * @param method 请求方式
     * @returns {string|string|*}
     */
    CompanyDynamicSetWhiteIp(key, trade_no, ips, method = 'POST') {
        let apiUrl = utils.getUrl(endpoint.COMPANY_DYNAMIC_SETWHITEIP);
        let params = {key, trade_no, ips};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 获取白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param method 请求方式
     * @returns {string|string|*}
     */
    CompanyDynamicGetWhiteIp(key,trade_no,method = 'POST'){
        let apiUrl = utils.getUrl(endpoint.COMPANY_DYNAMIC_GETWHITEIP);
        let params = {key, trade_no};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }

    /**
     * 按量付费 -- 替换白名单
     * @param key 业务密钥
     * @param trade_no 业务号
     * @param del_ip 新的IP
     * @param method 请求方式
     * @returns {string|string|*}
     */
    CompanyDynamicDelWhiteIp(key,trade_no,del_ip,method = 'POST'){
        let apiUrl = utils.getUrl(endpoint.COMPANY_DYNAMIC_DELWHITEIP);
        let params = {key, trade_no, del_ip};
        params = utils.getQuery(params);
        return this.httpRequest(method, apiUrl, params);
    }


    /**
     * 处理请求
     * @param {String} method 请求方式 GET | METHOD
     * @param {String} endpoint 请求URL地址
     * @param {Object} params 请求参数对象
     * @returns
     */
    httpRequest(method, endpoint, params) {
        try {
            let url = endpoint;
            if (method === 'GET') {
                let resp = http('GET', url + '?' + params).getBody('UTF8');
                console.log(url + "?" + params);
                console.log(resp)
                return resp;
            } else if (method === 'POST') {
                let resp = http('POST', url + '?' + params, {'Content-Type': 'application/x-www-form-urlencoded'}).getBody('UTF8');
                console.log(url + "?" + params);
                return resp;
            } else {
                return 'Request method type error !!!'
            }
        } catch (e) {
            console.log('error msg : ' + e)
            return e;
        }

    }


}

module.exports = Client;